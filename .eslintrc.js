module.exports = {
  ignorePatterns: ['node_modules', '.next', 'build', 'public'],
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'next'
  ],
  settings: {
    react: {
      version: 'detect',
    },
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    semi: ['error', 'always'],
    'react/react-in-jsx-scope': 'off',
    'jsx-a11y/accessible-emoji': 'off',
  },
};

import '../styles/globals.css';
import '@fontsource/barlow';

import React from 'react';

import Index from './index';

// This default export is required in a new `pages/_app.js` file.
export default function Portfolio() {
  return <Index />;
}

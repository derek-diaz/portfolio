import React from 'react';

import { Education } from '../components/Education';
import { Experience } from '../components/Experience';
import { Header } from '../components/Header';
import { Footer } from '../components/Footer';
import { Info } from '../components/Info';
import { Links } from '../components/Links';
import { Meta } from '../components/Metatags';
import { Projects } from '../components/Projects';

const Index = () => {
  return (
    <React.Fragment>
      <Meta />
      <main className="app">
        <div className="main">
          <Header />
          <Info />
          <Experience />
          <Education />
          <Projects />
          <Links />
          <Footer />
        </div>
      </main>
    </React.Fragment>
  );
};

export default Index;

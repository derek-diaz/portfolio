const projects = [
  {
    title: "Parranda Boricua",
    image: "/parranda-boricua-cloud.jpg",
    description:
      "This application is designed to bring the traditional Puerto Rican Christmas 'parrandas' experience to users. This innovative application is equipped with the many tools and features to create a traditional parrandas right from the convenience of a user's device.",
    linkText: "View the live application",
    linkUrl: "https://parrandaboricua.com/",
    repoUrl: "",
    repoHost: ""
  },
  {
    title: "Scienteer",
    image: "/scienteer.jpg",
    description:
      "Scienteer is a software application that has been developed to assist students, teachers, and fair directors in adhering to the guidelines of the Intel ISEF competitions. This application was crafted utilizing technologies such as PHP, Laravel, Bootstrap, and MySQL, and has been designed to provide a streamlined and efficient experience for all users.",
    linkText: "Application",
    linkUrl: "https://scienteer.com/",
    repoUrl: "",
    repoHost: ""
  },
  {
    title: "SchlockDB",
    image: "/schlockdb.jpg",
    description:
      "SchlockDB is a professional database of unique and distinctive films, which may be considered of questionable quality. The database is designed to provide a vast selection of movies to cater to a diverse audience, with a focus on films that are outside of the mainstream.",
    linkText: "Application",
    linkUrl: "",
    repoUrl: "https://gitlab.com/schlockdb",
    repoHost: "gitlab"
  },
  {
    title: "Scienteer Mobile",
    image: "/scienteer-mobile.jpg",
    description:
      "This is the mobile version of Scienteer. Completely built with Ionic 3, Cordova and Angular.",
    linkText: "",
    linkUrl: "",
    repoUrl: "https://gitlab.com/derek-diaz/scienteer-mobile",
    repoHost: "gitlab"
  },
  {
    title: "COVID-19 Tracking Map",
    image: "/covid19.jpg",
    description:
      "This dashboard was designed to provide insight into the spread of COVID-19 within the city of San Antonio. This powerful tool was created with the goal of helping the community stay informed and make informed decisions in regards COVID-19.",
    linkText: "Application",
    linkUrl: "https://covid19-satx.netlify.com/",
    repoUrl: "https://github.com/derek-diaz/COVID-19_SATX",
    repoHost: "github"
  },
  {
    title: "Self-Driving Pickup Truck",
    image: "/self-driving.jpg",
    description:
      "I successfully transformed my pickup truck into an autonomous vehicle using a modified version of Comma.ai's OpenPilot source code. The modifications were specifically tailored to support the unique specifications of my pickup truck, enabling it to function as a self-driving vehicle.",
    linkText: "Video",
    linkUrl: "https://www.youtube.com/watch?v=RjdYqoBsdfQ",
    repoUrl: "",
    repoHost: ""
  },
  {
    title: "El Bad Robot",
    image: "/el-bad-robot.jpg",
    description:
      "El Bad Robot is a simple AI that is trained from Bad Bunny lyrics. In theory it should be able to write music lyrics.",
    linkText: "",
    linkUrl: "",
    repoUrl: "https://github.com/derek-diaz/el-bad-robot",
    repoHost: "github"
  },
  {
    title: "VisiPattern",
    image: "/visi-pattern.jpg",
    description:
      "VisiPattern is a professional software application that was developed as part of a Master's Thesis. This innovative tool generates procedural diagrams for Common Vulnerabilities and Exposures (CVEs) to enhance the understanding of the vulnerabilities and their impact, providing an efficient and effective way to evaluate security threats.",
    linkText: "",
    linkUrl: "",
    repoUrl: "https://gitlab.com/derek-diaz/visipattern",
    repoHost: "gitlab"
  },
  {
    title: "Fractal",
    image: "/fractal.jpg",
    description: "Video-game developed in Unity for a 3D graphics course for College.",
    linkText: "",
    linkUrl: "",
    repoUrl: "https://gitlab.com/derek-diaz/fractal",
    repoHost: "gitlab"
  },
  {
    title: "Living Snap",
    image: "/living-snap.jpg",
    description:
      "LivingSnap is a cloud-based application that utilizes image metadata to generate dynamic and interactive maps with photos.",
    linkText: "",
    linkUrl: "",
    repoUrl: "https://gitlab.com/derek-diaz/livingsnap-android",
    repoHost: "gitlab"
  },
  {
    title: "Tales of Awerim",
    image: "/tales-of-awerim.jpg",
    description:
      "This was a video game developed as part of a competition organized by Microsoft. The game was created utilizing the Windows Phone 7 Platform and XNA framework. Our game received recognition by placing 2nd out of 15 entries in the competition.",
    linkText: "",
    linkUrl: "",
    repoUrl: "",
    repoHost: ""
  }
];

export default projects;

const workExperience = [
  {
    company: 'Plus One Robotics',
    title: 'Tech Lead & Senior Software Developer 2',
    location: 'San Antonio, TX',
    image: '/por.jpg',
    time: 'September 2020 - Present',
  },
  {
    company: 'USAA',
    title: 'Software Developer and Integrator 1',
    location: 'San Antonio, TX',
    image: '/usaa.jpg',
    time: 'January 2014 - September 2020',
  },
  {
    company: 'Scienteer Technologies',
    title: 'Co-Founder and Developer',
    location: 'San Antonio, TX',
    image: '/scienteer.jpg',
    time: 'April 2015 - November 2019',
  },
  {
    company: 'The University of Texas At San Antonio',
    title: 'Graduate Research Assistant',
    location: 'San Antonio, TX',
    image: '/utsa.jpg',
    time: 'August 2013 - May 2014',
  },
  {
    company: 'Tesoro Corporation',
    title: 'Enterprise Architecture, Mobile Web Applications Developer - Intern',
    location: 'San Antonio, TX',
    image: '/tesoro.jpg',
    time: 'February 2013 - May 2014',
  },
  {
    company: 'XRM Group',
    title: 'Software Developer 2',
    location: 'Caguas, PR',
    image: '/xrm.jpg',
    time: 'February 2012 - June 2012',
  },
  {
    company: 'INDUNIV Research Consortium',
    title: 'Web Developer',
    location: 'San Juan, PR',
    image: '/induniv.jpg',
    time: 'March 2009 - August 2011',
  },
];

export default workExperience;

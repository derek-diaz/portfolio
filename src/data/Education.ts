const education = [
  {
    university: 'The University of Texas at San Antonio',
    image: '/utsa.jpg',
    degree: 'Master of Science - Computer Science',
    graduation: '2015',
    linkText: '',
    linkUrl: '',
  },
  {
    university: 'University of Puerto Rico at Bayamon',
    image: '/uprb.jpg',
    degree: "Bachelor's Degree - Computer Science",
    graduation: '2012',
    linkText: '',
    linkUrl: '',
  },
];

export default education;

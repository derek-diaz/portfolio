/* eslint-disable @next/next/no-img-element,react/prop-types */
import {
  faGithub,
  faGitlab,
  faLinkedin,
  faTwitter
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

export const Gitlab = () => (
  <a
    href="https://gitlab.com/derek-diaz"
    title="Gitlab Profile"
    className="project-repo-icon">
    <FontAwesomeIcon icon={faGitlab} className="h-8 w-8 m-2 text-white cursor-pointer" />
  </a>
);

export const GitlabProject = ({ url }: { url: string }) => (
  <a href={url} title="Gitlab Repository">
    <img
      alt="Gitlab"
      className="h-8 w-8 m-2 float-right"
      src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAFCklEQVRoge2YzXNTZRTGf+/9yG1yk7SVpB+U2qEUDP0AaqGgFRlm3LFutROVJRuXLhhXbnSGGcY9bp1u4H9wpjqiog4giLQFxqoQQgj9oi1tk/u6IGnzce9Nmo8ZHfusknPO+z7Pk3POTVrYwQ528P9AbORYX+z48d5/G49SbqElxailyrHKZJWP7fKUbQApR5Hy3YpUbQfb5CnLQGzkWB/QC0TqOUaV8JRlwJJidPN1HceoEp7yRkjK0ZzX9RujCnhKGshpaxZ1GaNKeUoayG3rZqwOY1QpT+kRym3rVqz2Y1Qhj6sBm7ZmUdMxqobH1YBdWzdzNRyjanjcR8iurVu52o1RFTyOBlzamkVNxqhaHkcDbm3drKnBGFXL4zxCbm3dqql+jKrk0WzrJ/Yf4RFJHgZLXR2JmydmUsvpnpIibODxqzOhPnN/ycLO+YT8aP8REZ25UZiy74DKWfYsDBNcu1Pq7sYO4+9yxNoh0GE8LFnkX5ti99JRVM7apYsMyEtDOlJEAYNIoglPOul2v6dJ2wfIMjXnwQhqe10LdGuO3ic+wIsUUXlpSC8sKe5AYOkMEAZAyN30x/9EyLQThxB0GkGtZKcK0RDUbqPQ5VwhLQZi9xF0ZgLhjLY8FBtQClqlpwc5kPzWTUyww3hahuY8BDoN187yWvIbdOuoqzYKDMjLPWEkRS5pWj1F69IPTlx6QD0IOHapCEJYml+NOOZDy9doWj1VFJeckZd7wrmh/A6k1ChQNGeAoGu+H9/6A3tBtHibtV9LK38Jb5N2U0CrfTL1B93PIoCwyeoZjZvIN6BI203PiPTT/wRU+dwu7e8wbON2CHR67GsVuUxfLIXA+fldoLFgB8TnwJzjYSG76YvbftK6qQ4IIdYcz2avUMS61qD02yZ74zdQcftOWUTyRW4gz4B4b+oKljoIOC+td+NN9s5N2mSafGHtpgv5y+Mh7QZCNBcluuYmMTdGXI7+iJoeEuPTX+UGi55CIvr7LOr0aRDngQ3bq1qej9D8okis2W6kShnw29U0rt6m7fkbDkfSwAUWAyfF2P17RXrdyOTEgWEUJsCurSLB9fY062pbTnAl9tOSlJY07e5TNLHcNhQA2Mp7UgmOxCyE7VLPAh+I8WnHiXD9e0BEp6+RtoaAL4uzMsyhx08Rcj0n6DNbncfI1+K5mSdesMFA/JGD+CtYxqCb+JIGAMT79xbF+PQ5hBijcMFVq59I4vvckNnaYPsDEcDX6sl/RB9MXEWzDheULQIfivHpMRG95fxAyaDsfy06Lnhw7RQdi99l36qGGFQ08azwvKoyr3nEltj2pasEXhR+Wdkuak0MgMuC71l4ncDa3cw73WzTfys862s3bgEeAMz1GV6dz/3kXRe1ZgYAxBhpMT51AYu3gCyZl0jCi2otAJitDf7Cc2aLEQBefhH2PVHZ2oVZ4LQYnz4vzv1i/9SrpYEsihZckV0cik8hkIrGYdUQj7O1mkeJKzoDCCQDj28jZHcmVdai1sUA2Cy4JzVMd3ISUPztxlS2ztztuQuo7EtOYqROsM1FrZuBLPIWPLTyNrtWfvaF9FA27wvpIV5Zuc6ulZNUsKhuqIkByFvwT+hJdgkzFdC86qxqKH8JX6qZnqcdwMVKFtWVt1YX5UJOHBgmrXy2+nVYwQLvO0nQU59WM+tOqIsBAHm5u3H9TvPHAJ7euYti7MFCvbh2sIMd/IfxDx+a0j0BUnaaAAAAAElFTkSuQmCC"
    />
  </a>
);

export const LinkedIn = () => (
  <a href="https://www.linkedin.com/in/derekdiazcorrea" title="LinkedIn Profile">
    <FontAwesomeIcon icon={faLinkedin} className="h-8 w-8 m-2 text-white cursor-pointer" />
  </a>
);

export const Github = () => (
  <a href="https://github.com/derek-diaz" title="Github Profile">
    <FontAwesomeIcon icon={faGithub} className="h-8 w-8 m-2 text-white cursor-pointer" />
  </a>
);

export const GithubProject = ({ url }: { url: string }) => (
  <a href={url} title="Github Repository">
    <FontAwesomeIcon icon={faGithub} className="h-8 w-8 m-2 text-white cursor-pointer float-right" />
  </a>
);

export const Twitter = () => (
  <a href="https://twitter.com/gigabit77" title="Twitter Profile">
    <FontAwesomeIcon icon={faTwitter} className="h-8 w-8 m-2 text-white cursor-pointer" />
  </a>
);

export const Email = () => (
  <a href="mailto:derek.diaz@protonmail.com">
    <FontAwesomeIcon icon={faEnvelope} className="h-8 w-8 m-2 text-white cursor-pointer" />
  </a>
);

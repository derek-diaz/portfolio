import React from "react";

import education from "../data/Education";

import ExpCard from "./ExpCard";

export const Education = () => {
  return (
    <React.Fragment>
      <h1 id="education" className="text-gray-300 text-3xl font-bold py-5 text-center">
        Where I got my education:
      </h1>
      <div className="max-w-6xl m-auto">
        <div className="grid justify-items-center sm:grid-cols-1 lg:grid-cols-3 md:grid-cols-2 items-center gap-4 px-5">
          {education.map((e) => (
            <ExpCard key={e.university} header={e.university} degree={e.degree} graduationYear={e.graduation} image={e.image} />
          ))}
        </div>
      </div>
    </React.Fragment>
  );
};

import React from "react";

export const Links = () => {
  return (
    <div className="max-w-4xl m-auto py-10">
      <h1 id="links" className="text-gray-300 text-3xl font-bold py-5 text-center">
        Impressed? Then let{"'"}s connect!
      </h1>
    </div>
  );
};

import React, { FC } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar, faGraduationCap, faIdBadge, faLaptopCode, faLocationDot } from "@fortawesome/free-solid-svg-icons";

const ExpCard: FC<ExpCardProps> = (props) => {
  return (
    <div
      className="bg-slate-800 max-w-sm h-full overflow-hidden rounded-xl shadow-lg transition-transform transform hover:scale-105 w-full">
      <img src={props.image} alt={props.header} className="h-48 w-full object-cover" />
      <div className="h-auto p-5 flex flex-col justify-between">
        <h2 className="text-white text-2xl font-bold mb-3">{props.header}</h2>

        {props.degree &&
					<p className="m-2 text-white flex items-center">
						<FontAwesomeIcon icon={faLaptopCode} className="mr-2" />
            {props.degree}
					</p>
        }
        {props.graduationYear &&
					<p className="m-2 text-white flex items-center">
						<FontAwesomeIcon icon={faGraduationCap} className="mr-2" />
            {props.graduationYear}
					</p>
        }
        {props.title &&
					<p className="m-2 text-white flex items-center">
						<FontAwesomeIcon icon={faIdBadge} className="mr-2" />
            {props.title}
					</p>
        }
        {props.period &&
					<p className="m-2 text-white flex items-center">
						<FontAwesomeIcon icon={faCalendar} className="mr-2" />
            {props.period}
					</p>
        }
        {props.location &&
					<p className="m-2 text-white flex items-center">
						<FontAwesomeIcon icon={faLocationDot} className="mr-2" />
            {props.location}
					</p>
        }
      </div>
    </div>
  );
};

interface ExpCardProps {
  title?: string;
  image: string;
  header?: string;
  period?: string;
  location?: string;
  degree?: string;
  graduationYear?: string;
}

export default ExpCard;

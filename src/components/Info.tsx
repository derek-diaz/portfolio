import React from "react";
import { TypeAnimation } from "react-type-animation";

export const Info = () => {
  return (
    <React.Fragment>
      <div className="grid sm:grid-cols-1 md:grid-cols-1 sm:m-1 md:m-3 lg:mx-36 items-center justify-center">
        <div>
          <h1 className="text-4xl mt-7 mb-7 text-gray-300 font-bold">
            <TypeAnimation
              sequence={["Wepa! 🎉🎉🎉", 800, "Hey I'm Derek Diaz Correa!", 1000]}
              speed={50}
              style={{ fontSize: "1em" }}
              wrapper="span"
              repeat={0}
              className="text-white text-xl p-2 m-2"
            />
          </h1>
          <h3 className="text-xl p-2 m-2 text-gray-300">
            Software Engineering Team Lead with 15+ years of experience in architecting and developing scalable systems.
            Passionate about technology, consistently delivering innovative solutions both in professional and personal
            projects.
          </h3>
        </div>
      </div>
    </React.Fragment>
  );
};

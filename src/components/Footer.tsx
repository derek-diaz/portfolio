import React from "react";

import { Email, Github, Gitlab, LinkedIn, Twitter } from "./Icons";

export const Footer = () => {
  return (
    <footer className="bg-zinc-800 p-4 flex flex-col items-center">
      <nav className="flex space-x-4 mt-2">
        <Gitlab />
        <Github />
        <LinkedIn />
        <Twitter />
      </nav>
      <div className="text-gray-400 mt-4">
        <small>&copy; {new Date().getFullYear()} Derek Diaz Correa. All rights reserved.</small>
      </div>
    </footer>
  );
};

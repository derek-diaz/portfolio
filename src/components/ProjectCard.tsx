import React, { FC } from "react";
import { GithubProject, GitlabProject } from "./Icons";

const ProjectCard: FC<ProjectCardProps> = (props) => {
  return (
    <React.Fragment>
      <div
        className="bg-slate-800 max-w-sm overflow-hidden rounded-xl shadow-lg transition-transform transform hover:scale-105 w-full h-full flex flex-col">
        <img src={props.project.image} alt={props.project.title} className="h-48 w-full object-cover" />
        <div className="flex-grow p-5 flex flex-col justify-between">
          <div>
            <h2 className="text-white text-2xl font-bold">{props.project.title}</h2>
            <p className="m-2 text-white">
              {props.project.description}
            </p>
          </div>
          <div className="mt-4">
            {props.project.linkUrl && props.project.linkUrl.length > 0 &&
							<a href={props.project.linkUrl} className="block text-white align-bottom">
                {props.project.linkText}
							</a>
            }
            {generateRepoIcon(props.project.repoHost, props.project.repoUrl)}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

const generateRepoIcon = (host: string, url: string) => {
  if (host) {
    if (host === "github") {
      return <GithubProject url={url} />;
    } else {
      return <GitlabProject url={url} />;
    }
  }
};

interface ProjectCardProps {
  project: {
    title: string;
    description: string;
    image: string;
    linkUrl: string;
    linkText: string;
    repoHost: string;
    repoUrl: string;
  };
}

export default ProjectCard;

import React from "react";

import workExperience from "../data/workExperience";
import ExpCard from "./ExpCard";

export const Experience = () => {
  return (
    <React.Fragment>
      <h1 id="experience" className="text-gray-300 text-3xl font-bold py-5 text-center">
        Here are some places I{"'"}ve worked:
      </h1>
      <div className="max-w-6xl m-auto">
        <div className="grid justify-items-center sm:grid-cols-1 lg:grid-cols-3 md:grid-cols-2 items-center gap-4 px-5">
          {workExperience.map((e) => (
            <ExpCard key={e.company} header={e.company} image={e.image} title={e.title} period={e.time}
                     location={e.location} />
          ))}
        </div>
      </div>
    </React.Fragment>
  );
};

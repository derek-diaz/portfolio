import React from "react";

import { Email, Github, Gitlab, LinkedIn, Twitter } from "./Icons";

export const Header = () => {
  return (
    <React.Fragment>
      <nav className="bg-zinc-800 p-2 flex flex-col md:flex-row items-center justify-between">
        <div className="text-white text-2xl p-2">DEREK DIAZ CORREA</div>
      </nav>
    </React.Fragment>
  );
};

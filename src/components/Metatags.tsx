/* eslint-disable react/no-unescaped-entities */
import React from "react";
import { Helmet } from "react-helmet";

export const Meta = () => (
  <Helmet>
      <meta charSet="utf-8" />
      <title>Derek's Portfolio</title>
      <meta name="description" content="Welcome to Derek's portfolio. Discover projects, experience, and more." />
      <meta name="robots" content="index, follow" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="og:title" content="Derek's Portfolio" />
      <meta name="og:description" content="Welcome to Derek's portfolio. Discover projects, experience, and more." />
      <meta name="og:type" content="website" />
      <link rel="apple-touch-icon" sizes="180x180" href="/public/apple-touch-icon.png" />
      <link rel="icon" type="image/png" sizes="32x32" href="/public/favicon-32x32.png" />
      <link rel="icon" type="image/png" sizes="16x16" href="/public/favicon-16x16.png" />
  </Helmet>
);

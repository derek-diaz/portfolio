import React from 'react';

import projects from '../data/projects';
import ProjectCard from "./ProjectCard";

export const Projects = () => {
  return (
    <React.Fragment>
      <h1 id="projects" className="text-gray-300 text-3xl font-bold py-5 text-center">
        Some of the amazing side projects I have worked on:
      </h1>
      <div className="max-w-6xl m-auto">
        <div className="grid justify-items-center sm:grid-cols-1 lg:grid-cols-3 md:grid-cols-2 items-center gap-4 px-5">
          {projects.map((project) => (
            <ProjectCard key={project.title} project={project} />
          ))}
        </div>
      </div>
    </React.Fragment>
  );
};
